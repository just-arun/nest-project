module.exports = {
    env: {
        BASE_URL: process.env.BASE_URL
    },
    webpack: (config, {
        buildId,
        dev,
        isServer,
        defaultLoaders,
        webpack
    }) => {
        new webpack.ProvidePlugin({
            React: 'react'
        })
        // Note: we provide webpack above so you should not `require` it
        // Perform customizations to webpack config
        config.plugins.push(new webpack.IgnorePlugin(/\/__test__\//))

        // Important: return the modified config
        return config
    },
}