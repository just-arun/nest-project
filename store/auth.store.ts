import { createCookie, getDataFromCookie } from './../services/http.config'
import { useRouter } from 'next/router'
import { sha512 } from 'js-sha512'
import { createContainer } from 'unstated-next'
import { useState } from 'react'
import AuthService from '../services/auth.service'
import Cookie from 'js-cookie'

interface AuthStateInterface {
  isLoggedIn: boolean
  user: {
    name: string
    email: string
  }
}

const initSate: AuthStateInterface = {
  isLoggedIn: false,
  user: {
    name: '',
    email: '',
  },
}

export default function useAuth(initialState = initSate) {
  const [state, setState] = useState(initialState)
  const router = useRouter()

  const login = async ({
    email,
    password,
  }: {
    email: string
    password: string
  }) => {
    try {
      const pwd = sha512(password)
      await AuthService.login({ email, password: pwd })
      const user = { name: 'User name', email: 'user@mail.com' }
      createCookie('user', user)
      getDataFromCookie('user')
      setState({ isLoggedIn: true, user })
      return router.push('/products')
    } catch (err) {
      alert(err)
    }
  }

  const updateUser = (value: AuthStateInterface) => {
    setState(value)
  }

  const logout = async () => {
    setState({ isLoggedIn: false, user: { name: '', email: '' } })
    Cookie.remove('user')
    Cookie.remove('x-access')
    router.push('/login')
  }

  return { ...state, login, logout, updateUser }
}

export const AuthState = createContainer(useAuth)
