import { createContainer } from 'unstated-next'
import { useState } from 'react'
import { Languages } from '../translations/languages'

const initSate: {
  lang: Languages,
} = {
  lang: Languages.English,
}

export default function useGlobal(initialState = initSate) {
  const [state, setState] = useState(initialState)

  /**
   * for updating language for application
   * @param language Language enum
   */
  const setLanguage = (language: Languages) =>
    setState({ ...state, ['lang']: language })

  return { ...state, setLanguage }
}

export const GlobalState = createContainer(useGlobal)
