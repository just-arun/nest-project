export default class Config {
    public PORT!: string;

    constructor() {
        this.init();
    }

    init() {
        this.PORT = process.env.BASE_URL;
    }
}