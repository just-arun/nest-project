export interface ProductInterface {
  id: number
  name: string
  description: Map<string, string>
  cost: number
  slug: string
  image: string
}

export class Product {
  id: number
  name: string
  slug: string
  description: Map<string, string>
  cost: number
  image: string
  constructor(data: ProductInterface) {
    this.id = !!data.id ? data.id : Date.now()
    this.name = data.name
    this.slug = data.name.replace(/ /g, '-')
    this.description = data.description
    this.cost = data.cost
    this.image = data.image
  }
}
