export enum Languages {
  Tamil = 'ta',
  English = 'en-us',
  French = 'fr',
}
