import { LangEn } from './keys'

export const EngToTam = {
  [LangEn.Name]: 'பெயர்',
  [LangEn.Description]: 'விளக்கம்',
  [LangEn.Cost]: 'விலை',
  [LangEn.Provider]: 'வழங்குநர்',
  [LangEn.Detail]: 'விவரம்',
  [LangEn.SearchProduct]: 'தேடல் தயாரிப்பு',
  [LangEn.Reviews]: 'மதிப்புரைகள்',
  [LangEn.LoginToContinue]: 'உள்ளடக்கத்தைக் காண உள்நுழைக',
  [LangEn.Email]: 'மின்னஞ்சல்',
  [LangEn.Password]: 'கடவுச்சொல்',
  [LangEn.EnterYourEmail]: 'மின்னஞ்சலை பதிவுசெய்',
  [LangEn.EnterYourPassword]: 'உங்கள் கடவுச்சொல்லை உள்ளிடவும்',
  [LangEn.Submit]: 'சமர்ப்பிக்கவும்',
  [LangEn.Logout]: 'வெளியேறு',
  [LangEn.Login]: 'உள்நுழைய',
  [LangEn.ViewProducts]: 'தயாரிப்புகளைக் காண்க',
}
