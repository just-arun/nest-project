import { LangEn } from './keys'

export const Eng = {
  [LangEn.Name]: LangEn.Name,
  [LangEn.Description]: LangEn.Description,
  [LangEn.Cost]: LangEn.Cost,
  [LangEn.Provider]: LangEn.Provider,
  [LangEn.Detail]: LangEn.Detail,
  [LangEn.SearchProduct]: LangEn.SearchProduct,
  [LangEn.Reviews]: LangEn.Reviews,
  [LangEn.LoginToContinue]: LangEn.LoginToContinue,
  [LangEn.Email]: LangEn.Email,
  [LangEn.Password]: LangEn.Password,
  [LangEn.EnterYourEmail]: LangEn.EnterYourEmail,
  [LangEn.EnterYourPassword]: LangEn.EnterYourPassword,
  [LangEn.Submit]: LangEn.Submit,
  [LangEn.Logout]: LangEn.Logout,
  [LangEn.Login]: LangEn.Login,
  [LangEn.ViewProducts]: LangEn.ViewProducts,
}
