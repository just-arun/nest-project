import { LangEn } from './keys'

export const EngToFre = {
  [LangEn.Name]: 'Nom',
  [LangEn.Description]: 'la description',
  [LangEn.Cost]: 'Coût',
  [LangEn.Provider]: 'fournisseuse',
  [LangEn.Detail]: 'détail',
  [LangEn.SearchProduct]: 'rechercher un produit',
  [LangEn.Reviews]: 'Commentaires',
  [LangEn.LoginToContinue]: 'connectez-vous pour voir le contenu',
  [LangEn.Email]: 'E-mail',
  [LangEn.Password]: 'Mot de passe',
  [LangEn.EnterYourEmail]: 'Entrer votre Email',
  [LangEn.EnterYourPassword]: 'Tapez votre mot de passe',
  [LangEn.Submit]: 'Nous faire parvenir',
  [LangEn.Login]: 'connexion',
  [LangEn.Logout]: 'Se déconnecter',
  [LangEn.ViewProducts]: 'Voir les produits',
}
