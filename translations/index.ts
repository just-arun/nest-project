import { Eng } from './languages/english'
import { EngToFre } from './languages/french'
import { EngToTam } from './languages/tamil'
import { Languages } from './languages'

export const Translations = (lang: Languages) => {
  switch (lang) {
    case Languages.Tamil:
      return EngToTam
    case Languages.French:
      return EngToFre
    default:
      return Eng
  }
}
