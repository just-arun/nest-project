import { useRouter } from 'next/router'
import { useEffect } from 'react'
import { getDataFromCookie } from '../services/http.config'
import { AuthState } from '../store/auth.store'
import React from 'react';

export default function AuthMiddleware({ children }) {
  const authState = AuthState.useContainer()
  const router = useRouter()

  useEffect(() => {
    const user = getDataFromCookie('user')
    if (!!user) {
      authState.updateUser({ isLoggedIn: true, user: user })
    } else {
      router.push('/login')
    }
    return
  }, [authState.isLoggedIn])

  return <div>{authState.isLoggedIn ? children : <></>}</div>
}
