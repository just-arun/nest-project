import React from 'react'
import { shallow } from 'enzyme'
import { GlobalState } from '../../../store/global.store'
import ProductPage from '../[slug]'

describe('Testing Product page', () => {
  const content = shallow(
    <GlobalState.Provider>
      <ProductPage></ProductPage>
    </GlobalState.Provider>,
  )
  it('It should be defined', () => {
    expect(content).toBeDefined()
  })
  it('It should have props', () => {
    expect(content.props.name).toBe('props')
  })
})
