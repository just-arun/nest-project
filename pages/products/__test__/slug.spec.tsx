import React from 'react'
import { shallow } from 'enzyme'
import { GlobalState } from '../../../store/global.store'
import ProductPage from '../[slug]'

describe('Testing slug', () => {
  const content = shallow(
    <GlobalState.Provider>
      <ProductPage></ProductPage>
    </GlobalState.Provider>,
  )
  it('It should have content', () => {
    expect(content.exists()).toBeTruthy()
  })
  it('It should be have head', () => {
    expect(content.find('head')).toBeDefined()
  })
})
