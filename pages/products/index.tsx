import { ProductInterface } from '../../classes/products'
import AppHead from '../../components/app-head/app-head.component'
import ProductComponent from '../../components/product/product.component'
import AuthMiddleware from '../../middlewares/auth.middleware'
import ProductService from '../../services/product.service'
import style from './products.module.scss'
import React from 'react';

export default function ProductsPage(props) {
  return (
    <AuthMiddleware>
      <AppHead
        title="Hot selling products | Nest app"
        description="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Rerum tenetur repellat cupiditate illum asperiores molestias impedit. Velit, commodi sapiente! Voluptatem deleniti distinctio illum molestiae magni inventore quas autem animi maxime!"
      />
      <div className={style.productsPage}>
        <div className={style.productsGrid}>
          {props.products.map((res: ProductInterface) => (
            <ProductComponent
              key={`product-${res.id}`}
              name={res.name}
              slug={res.slug}
              image={res.image}
              description={res.description}
              cost={res.cost}
              id={res.id}
            />
          ))}
        </div>
      </div>
    </AuthMiddleware>
  )
}

ProductsPage.getInitialProps = async (_ctx) => {
  const products = await ProductService.getProducts()
  return { products }
}
