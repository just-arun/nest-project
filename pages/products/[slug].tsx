import AppHead from '../../components/app-head/app-head.component'
import ReviewComponent from '../../components/product/review/review.component'
import ProductService from '../../services/product.service'
import { GlobalState } from '../../store/global.store'
import { Translations } from '../../translations'
import { LangEn } from '../../translations/languages/keys'
import style from './product.module.scss'
import React from 'react';

export default function ProductPage(props) {
  const globalState = GlobalState.useContainer()
  const translate = Translations(globalState.lang)
  return (
    <div>
      <AppHead title={props.name} description={props.description}></AppHead>

      <div className={style.detailContent}>
        <img src={props.image} alt="image" />
        <div className={style.textContainer}>
          <h1>
            <div className={style.caption}>{translate[LangEn.Name]}</div>
            <div className="text">{props.name}</div>
          </h1>
          <div>
            <div className={style.caption}>{translate[LangEn.Description]}</div>
            <p className="text">{props.description[globalState.lang]}</p>
          </div>
          <div>
            <div className={style.caption}>{translate[LangEn.Cost]}</div>
            <div>Rs.{props.cost}</div>
          </div>
        </div>
      </div>
      <h1 className={style.reviewsTitle}>{translate[LangEn.Reviews]}</h1>
      <div className={style.reviewList}>
        {[0, 1, 2, 3, 4].map((_) => (
          <ReviewComponent />
        ))}
      </div>
    </div>
  )
}

ProductPage.getInitialProps = async (ctx) => {
  const data = await ProductService.getOne(ctx.query.slug)
  return data
}
