// mock api for frontend

export class ProductApi {

  constructor() {
    this.products = [];
    for (let i = 0; i < 12; i++) {
      let data = {
        id: Date.now() + i,
        name: 'some product ' + i,
        slug: 'some-product-' + i,
        image: `/product/${i + 1}.jpg`,
        description: {
          'en-us':
            'Lorem ipsum dolor sit amet consectetur adipisicing elit. Et rerum rem laborum architecto similique quas ullam ut dolores minima laboriosam quis blanditiis neque, amet iure atque deleniti provident repellat itaque.',
          ta:
            'எட் ரெரம் ரெம் லேபரம் ஆர்கிடெக்டோ சிமிலிக் குவாஸ் உலாம் உட் டோலோரஸ் மினிமா லேபிரியோசம் க்விஸ் பிளாண்டிட்டிஸ் நெக், அமெட் ஐயர் அட் டெலெனிட்டி ப்ராவிடன்ட் ரிப்பல்லட் இட்டாக்.',
          fr:
            "Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un imprimeur anonyme.",
        },
        cost: Number(Math.random() * 999).toFixed(2),
      }
      this.products.push(data)
    }
  }

  getProducts(req, res) {
    if (req.query.slug) {
      return this.getOne(req, res)
    }
    if (req.query.search) {
      return this.getFilteredProduct(req, res)
    }
    return res.status(200).json({ products: this.products })
  }

  getFilteredProduct(req, res) {
    let search = req.query.search
    console.log(search)

    let products = this.products.filter((pro) => {
      const condition_1 = String(pro.name).indexOf(search) != -1
      const condition_2 = String(pro.description).indexOf(search) != -1
      if (condition_1 || condition_2) {
        return pro
      }
    })
    console.log(products.length)
    return res.status(200).json({ products })
  }

  getOne(req, res) {
    let product = this.products.find((itm) => itm.slug == req.query.slug)
    return res.status(200).json({ product: product })
  }

  updateOne(req, res) {
    this.products = this.products.map((product) => {
      if (product.id == req.query.id) {
        return req.body
      }
    })
    return res.status(200).json({ data: 'ok' })
  }

  createOne(req, res) {
    this.products.push(req.body)
    return res.status(200).json({ product: req.body })
  }

  deleteOne(req, res) {
    this.products = this.products.map((product) => {
      if (product.id !== req) {
        return product
      }
    })
    return res.status(200).end()
  }
}

export default (req, res) => {
  let products = new ProductApi()
  switch (req.method) {
    case 'GET':
      return products.getProducts(req, res)
    case 'PUT':
      return products.updateOne(req, res)
    case 'POST':
      return products.createOne(req, res)
    case 'DELETE':
      return products.deleteOne(req, res)
    default:
      return products.getProducts(req, res)
  }
}
