import { sha512 } from 'js-sha512'

export default (req, res) => {
  const email = 'user@mail.com'
  const pwd = sha512('pwd123').toString()
  if (req.body.email !== email || req.body.password !== pwd) {
    return res.status(401).json({ error: { message: 'Invalid credentials' } })
  }
  res.setHeader('x-access', 'this is hashed access token')
  return res.status(200).json({ data: 'ok' })
}
