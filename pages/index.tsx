import { useRouter } from 'next/router'
import AppHead from '../components/app-head/app-head.component'
import { GlobalState } from '../store/global.store'
import styles from '../styles/Home.module.css'
import { Translations } from '../translations'
import { LangEn } from '../translations/languages/keys'
import React from 'react';

export default function Home() {
  const globalState = GlobalState.useContainer()
  const translation = Translations(globalState.lang)
  const router = useRouter()

  const goToProducts = () => {
    router.push('/products')
  }

  return (
    <div className={styles.container}>
      <AppHead
        title="Nest app"
        description="this sight is for tech demonstration"
      />
      <main className={styles.main}>
        <button
          onClick={goToProducts}
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"
        >
          {translation[LangEn.ViewProducts]}
        </button>
      </main>
    </div>
  )
}
