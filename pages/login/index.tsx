import React, { useState } from 'react'
import AppHead from '../../components/app-head/app-head.component'
import TextInputComponent from '../../components/ui/text-input/text-input.component'
import { GlobalState } from '../../store/global.store'
import { Translations } from '../../translations'
import { LangEn } from '../../translations/languages/keys'
import style from './login.module.scss'
import { AuthState } from '../../store/auth.store'

export default function LoginPage() {
  const globalState = GlobalState.useContainer()
  const authState = AuthState.useContainer()
  const translation = Translations(globalState.lang)
  const [email, setEmail] = useState('user@mail.com')
  const [password, setPassword] = useState('pwd123')

  const login = async () => {
    await authState.login({ email, password })
  }
  return (
    <div>
      <AppHead title="Login | Nest App" description="login to Nest app" />
      <div className={style.loginLayout}>
        <form
          onSubmit={(e) => {
            e.preventDefault()
            login()
          }}
          className={style.loginForm}
        >
          <h1 className={style.loginTitle}>Login</h1>
          <p className={style.description}>
            Login to view products credentials email: user@mail.com, password:
            pwd123
          </p>
          <TextInputComponent
            label={translation[LangEn.Email]}
            placeholder={translation[LangEn.EnterYourEmail]}
            inputId={'email'}
            onChange={(val) => setEmail(val.toLocaleLowerCase())}
            type="email"
            value={email}
          />
          <TextInputComponent
            label={translation[LangEn.Password]}
            placeholder={translation[LangEn.EnterYourPassword]}
            inputId={'password'}
            onChange={setPassword}
            type="password"
            value={password}
          />
          <button className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            {translation[LangEn.Submit]}
          </button>
        </form>
      </div>
    </div>
  )
}
