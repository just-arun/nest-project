import '../styles/globals.css'
import 'tailwindcss/tailwind.css'
import { GlobalState } from '../store/global.store'
import DashboardLayout from '../components/layouts/dashboard/dashboard.layout'
import { AuthState } from '../store/auth.store'
import React from 'react';

function MyApp({ Component, pageProps }) {  
  return (
    <AuthState.Provider>
      <GlobalState.Provider>
        <DashboardLayout>
          <Component {...pageProps} />
        </DashboardLayout>
      </GlobalState.Provider>
    </AuthState.Provider>
  )
}

export default MyApp
