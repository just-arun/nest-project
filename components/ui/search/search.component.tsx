import style from './search.module.scss'
import { useRouter } from 'next/router'
import { useState } from 'react'
import { GlobalState } from '../../../store/global.store'
import React from 'react';

interface SuggestionsInterface {
  name: string
  description: string
  link: string
}

interface SearchComponentInterface {
  placeholder: string
  onChange: (val) => void
  suggestions: SuggestionsInterface[]
}

export default function SearchComponent({
  placeholder,
  onChange,
  suggestions,
}: SearchComponentInterface) {
  const router = useRouter()
  const [input, setInput] = useState('')

  const updateInput = (val) => {
    setInput(val)
    onChange(val)
  }

  const updatePath = (path: string) => {
    router.push(path)
    setInput('')
    onChange('')
  }

  const globalState = GlobalState.useContainer()
  return (
    <div className={style.searchComponent}>
      <input
        value={input}
        onChange={({ target }) => updateInput(target.value)}
        className={style.input}
        type="text"
        placeholder={placeholder}
      />
      {input.length > 2 ? (
        <ul className={style.suggestions}>
          {suggestions.map((itm, i) => (
            <li key={i} className={style.suggestionsList}>
              <div
                onClick={() => updatePath(itm.link)}
                className={style.suggestionText}
              >
                <h4 className={style.suggestionTitle}>{itm.name}</h4>
                <p className={style.suggestionDescription}>
                  {itm.description[globalState.lang]}
                </p>
              </div>
            </li>
          ))}
        </ul>
      ) : (
        <></>
      )}
    </div>
  )
}
