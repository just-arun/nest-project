import style from './text-input.module.scss'
import React from 'react';

export default function TextInputComponent(props: {
  label: string
  placeholder: string
  inputId: string
  type: 'text' | 'password' | 'email'
  value: string
  onChange: (val: string) => void
}) {
  return (
    <div className={style.textInput}>
      <label className={style.caption} htmlFor={props.inputId}>
        {props.label}
      </label>
      <input
        id={props.inputId}
        className={style.input}
        value={props.value}
        onChange={({ target }) => props.onChange(target.value)}
        type={props.type}
        placeholder={props.placeholder}
      />
    </div>
  )
}
