import React from 'react'
import { shallow } from 'enzyme'
import SelectComponent from './select.component'

let initial = {
  label: 'label 1',
  value: 'value 1',
}
let valueList = [
  { label: 'label 1', value: 'value 1' },
  { label: 'label 2', value: 'value 2' },
  { label: 'label 3', value: 'value 3' },
  { label: 'label 4', value: 'value 4' },
]

describe('Select Component', () => {
  const container = shallow(
    <SelectComponent
      item={initial}
      items={valueList}
      onChange={() => {}}
    ></SelectComponent>,
  )
  it('It should be defined', () => {
    expect(container).toBeDefined()
  })

  it('It should have length 1', () => {
    expect(container.length).toBe(1)
  })

  it('It should have Button', () => {
    expect(container.exists('button')).toBe(true)
  })

  it('It should show value "value 1"', () => {
    expect(container.find('button').childAt(0).childAt(0).text()).toBe(
      'label 1',
    )
  })

  it('It should have unordered list', () => {
    expect(container.find('ul').length).toBe(1)
  })

  it('It should have 4 list items', () => {
    expect(container.find('li').length).toBe(4)
  })
})
