import { useCallback, useRef, useState } from 'react'
import style from './select.module.scss'
import Image from 'next/image'
import React from 'react';

interface SelectComponentProps {
  item: ItemInterface
  items: ItemInterface[]
  onChange: (value: ItemInterface) => void
}

interface ItemInterface {
  label: string
  value: any
}

export default function SelectComponent({
  item,
  items,
  onChange,
}: SelectComponentProps) {
  const [itm, setItm] = useState(item)
  const btnRef = useRef(null)

  const onChangeValue = useCallback((val) => {
    setItm(val)
    onChange(val)
    btnRef.current.blur()
  }, [])

  return (
    <div>
      <button ref={btnRef} className={style.selectComponent}>
        <div className={style.selectComponentValue}>
          <span className={style.selectComponentValueText}>{itm.label}</span>
          <Image
            className={style.selectComponentIcon}
            src="/expand-arrow.png"
            alt="arrow"
            width={12}
            height={12}
          />
        </div>
        <ul className={style.selectComponentList}>
          {items.map((res, i) => (
            <li
              className={style.selectComponentListItem}
              key={`select-${i}`}
              onClick={() => onChangeValue(res)}
            >
              <span>{res.label}</span>
            </li>
          ))}
        </ul>
      </button>
    </div>
  )
}
