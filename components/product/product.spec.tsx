import React from 'react'
import { render } from 'enzyme'
import { GlobalState } from './../../store/global.store'
import ProductComponent from './product.component'

describe('Product Component', () => {
  const description: any = { 'en-us': 'some description' }
  const content = render(
    <GlobalState.Provider>
      <ProductComponent
        cost={10}
        description={description}
        id={9304}
        image={'/product/1.jpg'}
        name="component name"
        slug="component-name"
      ></ProductComponent>
    </GlobalState.Provider>,
  )
  it('It should be defined', () => {
    expect(content).toBeDefined()
  })

  it('It should have image', () => {
    expect(content.find('img')).toBeTruthy()
  })

  it('Image should have path image', () => {
    expect(content.find('img').attr()['alt']).toBe('image')
  })

  it('Link should have class named text', () => {
    expect(content.find('div').hasClass('text')).toBe(true)
  })

  it('It should have p tag', () => {
    expect(content.find('p')).toBeDefined()
  })
})
