import style from './review.module.scss'
import StarsComponent from './stars/stars.component'
import React from 'react';


export default function ReviewComponent(_props) {
  return (
    <div className={style.comment}>
      <h1 className={style.title}>Fresh product taste good</h1>
      <div>
        <StarsComponent rating={3} />
      </div>
      <p className={style.reviews}>review</p>
    </div>
  )
}
