import { shallow } from 'enzyme'
import StarsComponent from './stars.component'

describe('Star component', () => {
  const content = shallow(<StarsComponent rating={3} />)
  it('It should be defined', () => {
    expect(content).toBeDefined()
  })
  it('It have image tag', () => {
    expect(content.find('img')).toBeDefined()
  })
  it('It should have five image tag', () => {
    expect(content.render().find('img').length).toBe(10)
  })
})
