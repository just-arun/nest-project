import style from './stars.module.scss'
import Image from 'next/image'
import React from 'react';

export default function StarsComponent({ rating }: { rating: number }) {
  let fullStars = new Array(rating).fill(0)
  let noStars = new Array(5 - rating).fill(0)
  return (
    <div className={style.wrapper}>
      {fullStars.map((_, i) => (
        <Image
          key={`full-star-${i}`}
          src="/product/ratings/star-full.svg"
          alt="full-star"
          height={15}
          width={15}
        />
      ))}
      {noStars.map((_, i) => (
        <Image
          key={`empty-star-${i}`}
          src="/product/ratings/star-empty.svg"
          alt="empty-star"
          height={15}
          width={15}
        />
      ))}
    </div>
  )
}
