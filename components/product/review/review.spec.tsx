import React from 'react'
import { shallow } from 'enzyme'
import ReviewComponent from './review.component'

describe('Testing Review component', () => {
  const content = shallow(<ReviewComponent />)
  it('It should be defined', () => {
    expect(content).toBeDefined()
  })
  it('It should have h1 tag', () => {
    expect(content.find('h1')).toBeDefined()
  })
  it('It should have one h1 tag', () => {
    expect(content.find('h1').length).toBe(1)
  })
  it('h1 should have expected text in it', () => {
    expect(content.find('h1').text()).toBe('Fresh product taste good')
  })
  it('It should have p tag', () => {
    expect(content.find('p')).toBeDefined()
  })
  it('It should have one p tag', () => {
    expect(content.find('p').length).toBe(1)
  })
  it('paragraph should have expected text content', () => {
    expect(content.find('p').text()).toBe('review')
  })
  it('It should have two div tags', () => {
    expect(content.find('div').length).toBe(2)
  })
})
