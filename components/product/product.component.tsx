import { ProductInterface } from '../../classes/products'
import { GlobalState } from '../../store/global.store'
import { Translations } from '../../translations'
import { LangEn } from '../../translations/languages/keys'
import style from './product.module.scss'
import Link from 'next/link'
import Image from 'next/image'
import React from 'react';

export default function ProductComponent({
  cost,
  description,
  name,
  slug,
  image,
}: ProductInterface) {
  const globalState = GlobalState.useContainer()
  const translate = Translations(globalState.lang)
  return (
    <Link href={`/products/${slug}`}>
      <div className={style.productComponent}>
        <div className={style.imageContainer}>
          <Image
            className={style.image}
            src={image}
            alt="image"
            layout="fill"
          />
        </div>
        <div className={style.padding}>
          <div className={style.name}>
            <div className={style.caption}>{translate[LangEn.Name]}</div>
            <div className="text">{name}</div>
          </div>
          <div className={style.description}>
            <div className={style.caption}>{translate[LangEn.Description]}</div>
            <p className={style.descriptionPara}>
              {description[globalState.lang]}
            </p>
          </div>
          <div className={style.cost}>
            <div className={style.caption}>{translate[LangEn.Cost]}</div>
            <div className="text">Rs.{cost}</div>
          </div>
        </div>
      </div>
    </Link>
  )
}
