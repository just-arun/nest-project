import React from 'react'
import Head from 'next/head'

export default function AppHead({
  title,
  description,
}: {
  title: string
  description: string
}) {
  return (
    <Head>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta charSet="utf-8" />
      <link rel="icon" href="/favicon.ico" />

      <meta property="og:title" content={`${title}`} key="ogtitle" />
      <meta property="og:description" content={description} key="ogdesc" />
      <title>{`${title} | Nest App`}</title>
      <meta name="description" content={description}></meta>
    </Head>
  )
}
