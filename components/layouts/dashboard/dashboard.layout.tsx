import Image from 'next/image'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { useCallback, useEffect, useState } from 'react'
import { AuthState } from '../../../store/auth.store'
import { GlobalState } from '../../../store/global.store'
import { Translations } from '../../../translations'
import { Languages } from '../../../translations/languages'
import { LangEn } from '../../../translations/languages/keys'
import SelectComponent from '../../ui/select/select.component'
import BaseLayout from '../base/base.layout'
import dashboardStyle from './dashboard.module.scss'
import React from 'react';


const DashboardLayout = ({ children }) => {
  const globalState = GlobalState.useContainer()
  const authState = AuthState.useContainer()
  const translation = Translations(globalState.lang)
  const router = useRouter()
  const [language, setLanguage] = useState({
    label: 'English',
    value: Languages.English,
  })

  const languageList = [
    { label: 'English', value: Languages.English },
    { label: 'Français', value: Languages.French },
    { label: 'தமிழ்', value: Languages.Tamil },
  ]

  useEffect(() => {
    for (let i = 0; i < languageList.length; i++) {
      const ele = languageList[i]
      if (ele.value == globalState.lang) {
        setLanguage(ele)
        return
      }
    }
    return () => {}
  }, [])

  const changeLanguage = useCallback((language) => {
    globalState.setLanguage(language.value)
  }, [])

  return (
    <BaseLayout>
      <div className={dashboardStyle.dashboard}>
        <header className={dashboardStyle.dashboardHeader}>
          <Link href="/">
            <Image
              className={dashboardStyle.dashboardLogo}
              src="/logo.svg"
              alt="picture"
              width={40}
              height={40}
            />
          </Link>
          <div className={dashboardStyle.dashboardDivider}></div>
          <div className={dashboardStyle.dashboardDivider}></div>
          <SelectComponent
            onChange={changeLanguage}
            item={language}
            items={languageList}
          />
          {authState.isLoggedIn ? (
            <button
              onClick={() => authState.logout()}
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mx-2"
            >
              {translation[LangEn.Logout]}
            </button>
          ) : (
            <button
              onClick={() => router.push('/login')}
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mx-2"
            >
              {translation[LangEn.Login]}
            </button>
          )}
        </header>
        <main className={dashboardStyle.dashboardMain}>{children}</main>
        <footer className={dashboardStyle.dashboardFooter}>
          <div>Nest app &#x24B8; 2021</div>
        </footer>
      </div>
    </BaseLayout>
  )
}

export default DashboardLayout
