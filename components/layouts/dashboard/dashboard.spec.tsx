import React from 'react'
import { shallow } from 'enzyme'
import DashboardLayout from './dashboard.layout'
import { GlobalState } from '../../../store/global.store'

describe('Testing Dashboard', () => {
  const content = shallow(
    <GlobalState.Provider>
      <DashboardLayout children={<></>} />
    </GlobalState.Provider>,
  )
  it('It should be defined', () => {
    expect(content).toBeDefined()
  })
  it('It should have header', () => {
    expect(content.find('header')).toBeDefined()
  })
  it('It should have footer', () => {
    expect(content.find('footer')).toBeDefined()
  })
  it('It should have main', () => {
    expect(content.find('main')).toBeDefined()
  })
})
