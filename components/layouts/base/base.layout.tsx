import { useEffect } from 'react'
import { GlobalState } from '../../../store/global.store'
import React from 'react';

const BaseLayout = ({ children }) => {
  const globalState = GlobalState.useContainer()
  useEffect(() => {
    document.documentElement.lang = globalState.lang
    return
  }, [globalState.lang])
  return <div>{children}</div>
}

export default BaseLayout
