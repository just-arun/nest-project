import React from 'react'
import { shallow } from 'enzyme'
import { GlobalState } from '../../../store/global.store'
import BaseLayout from './base.layout'

describe('Testing base component', () => {
  const content = shallow(
    <GlobalState.Provider>
      <BaseLayout>
        <div></div>
      </BaseLayout>
    </GlobalState.Provider>,
  )
  it('It should be defined', () => {
    expect(content).toBeDefined()
  })
  it('It should have div', () => {
    expect(content.find('div')).toBeDefined()
  })
  it('It should have one div', () => {
    expect(content.find('div').length).toBe(1)
  })
})
