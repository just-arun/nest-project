import AppHttp from './http.config'

export default class AuthService {
  public static async login(param: { email: string; password: string }) {
    return AppHttp.post(`/login`, param)
  }
}
