import axios from 'axios'
import cookie from 'js-cookie'
import Config from '../config/config'


export const createCookie = (name: string, val: any) => {
  cookie.set(name, JSON.stringify(val), { expires: 1, path: '/' })
}

export const getDataFromCookie = (name: string) => {
  let data = cookie.getJSON(name)
  return data
}

export const appConfig = new Config()

const AppHttp = axios.create({
  baseURL: appConfig.PORT,
})

AppHttp.interceptors.response.use(
  (res) => {
    if (res.status == 200) {
      if (res.headers['x-access']) {
        createCookie('x-access', res.headers['x-access'])
      }
      return res
    } else {
      if (res.status == 401 || res.status == 403) {
        window.location.pathname = '/'
      }
      // throw res;
    }
  },
  (err) => {
    return Promise.reject(err)
  },
)

export default AppHttp
