import AppHttp from './http.config'

export default class ProductService {
  public static async getProducts() {
    return AppHttp.get('/products')
      .then((res) => res.data.products)
      .catch((err) => Promise.reject(err))
  }

  public static async getOne(slug: string) {
    return AppHttp.get(`/products?slug=${slug}`)
      .then((res) => res.data.product)
      .catch((err) => Promise.reject(err))
  }
}
